import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {

    void food(String food) {
        int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to order "+food+"?","Order Confirmation", JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            textArea1.setText("Order for "+food +" received.");
        }
        JOptionPane.showMessageDialog(null,"Order for "+food+" received.");
    }
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextArea textArea1;

    public FoodGUI() {
        String food1 = "Tempura";
        String food2 = "Ramen";
        String food3 = "Udon";
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               food(food1);
            }
        });

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               food(food2);
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                food(food3);
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
