import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;


public class Report1 {


    public void checkout(){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to check out", "Checkout Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            JOptionPane.showMessageDialog(null, "Thank you the total price is " + receivedPrice.getText() + " yen");
            receivedPrice.setText("0");
            receivedInfo.setText(null);
        }
    }

    public void currentOrder(String food, int price){
        String currenText = receivedInfo.getText();
        receivedInfo.setText(currenText + "・" + food + " " + price + " yen\n");
    }

    public void totalprice(int price){
        int currentprice = Integer.parseInt(receivedPrice.getText());
        currentprice += price;
        receivedPrice.setText(Integer.toString(currentprice));
    }
    public void food(String food, int price) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to " + food + " " + price + "yen?",
                "Order Confirmation", JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            totalprice(price);
            currentOrder(food, price);
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering " + food + "!It will be served as soon as possible.");
        }
    }

        private JPanel root;
        private JLabel topLabel;
        private JButton cookieAndCreamChiffonButton;
        private JButton orangeAndMangoCakeButton;
        private JButton chocolateCocoaDanishButton;
        private JTextPane receivedInfo;
        private JButton mixedParfaitCakeButton;
        private JButton mochiriBallButton;
        private JButton strawberryDonutButton;
        private JButton checkOutButton;
        private JLabel receivedPrice;

    public Report1() {
            String food1 = "Cookie & cream chiffon cake";
            String food2 = "Orange & mango cake";
            String food3 = "Chocolate & cocoa danish";
            String food4 = "Mixed parfait cake";
            String food5 = "Mochiri Ball";
            String food6 = "Strawberry donut";



            int price1 = 455;
            int price2 = 525;
            int price3 = 305;
            int price4 = 590;
            int price5 = 370;
            int price6 = 290;

            receivedPrice.setText("0");



            /*アイコン設定*/
            cookieAndCreamChiffonButton.setIcon(new javax.swing.ImageIcon(".\\src\\images\\Cookie & cream chiffon cake.jpg"));
            orangeAndMangoCakeButton.setIcon(new javax.swing.ImageIcon(".\\src\\images\\Orange & mango cake.jpg"));
            chocolateCocoaDanishButton.setIcon(new javax.swing.ImageIcon(".\\src\\images\\Chocolate & cocoa danish.jpg"));
            mixedParfaitCakeButton.setIcon(new javax.swing.ImageIcon(".\\src\\images\\Mixed parfait cake.jpg"));
            mochiriBallButton.setIcon(new javax.swing.ImageIcon(".\\src\\images\\Mochiri Ball.jpg"));
            strawberryDonutButton.setIcon(new javax.swing.ImageIcon(".\\src\\images\\Strawberry donut.jpg"));


            cookieAndCreamChiffonButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    food(food1, price1);
                }
            });
            orangeAndMangoCakeButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    food(food2, price2);
                }
            });
            chocolateCocoaDanishButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    food(food3, price3);
                }
            });
            mixedParfaitCakeButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    food(food4, price4);
                }
            });
            mochiriBallButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    food(food5, price5);
                }
            });
            strawberryDonutButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    food(food6, price6);
                }
            });
            checkOutButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    checkout();
                }
            });
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("Report1");
        frame.setContentPane(new Report1().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
